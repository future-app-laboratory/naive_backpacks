package moe.fal.naive_backpacks.mixin;

import moe.fal.naive_backpacks.backpack.BackpackDataManager;
import moe.fal.naive_backpacks.backpack.ItemStackBackpack;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerInventory.class)
public class PlayerInventoryMixin {
    @Shadow
    @Final
    public PlayerEntity player;

    @Inject(at = @At("HEAD"), method = "insertStack(L net/minecraft/item/ItemStack;) Z", cancellable = true)
    private void init(ItemStack stack, CallbackInfoReturnable<Boolean> cir) {
        ItemStackBackpack itemStack = ItemStackBackpack.findIn(this.player.getInventory());
        MinecraftServer server = this.player.getServer();
        if (itemStack != null && server != null) {
            BackpackDataManager storage = new BackpackDataManager(server);
            BackpackDataManager.State state = storage.getOrCreate(itemStack);
            if (state.getUpgrades().isAutoCollectingEnabled() && state.getContent().insertItem(stack)) {
                cir.setReturnValue(true);
            }
        }
    }
}