package moe.fal.naive_backpacks.upgrade

interface StackMultiplierUpgrade {
    val multiplier: Int
}