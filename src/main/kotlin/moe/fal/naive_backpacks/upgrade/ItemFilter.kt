package moe.fal.naive_backpacks.upgrade

import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.nbt.NbtList
import net.minecraft.registry.Registries
import net.minecraft.util.Identifier

class ItemFilter(val ignoresNbt: Boolean = true, val isBlocklistMode: Boolean = true) {
    val patterns = arrayOfNulls<Pattern>(MAX_PATTERN_COUNT)


    fun matches(itemStack: ItemStack): Boolean = patterns
        .filterNotNull()
        .any { it.item == itemStack.item && (ignoresNbt || it.nbt == itemStack.nbt) }
        .xor(isBlocklistMode)

    operator fun set(index: Int, pattern: Pattern) {
        patterns[index] = pattern
    }

    operator fun set(index: Int, itemStack: ItemStack) {
        patterns[index] = Pattern(itemStack.item, itemStack.nbt)
    }


    fun toNbt(): NbtCompound = NbtBuilder.compoundOf {
        "IgnoresNbt" to ignoresNbt
        "IsBlocklistMode" to isBlocklistMode
        "Patterns" to listOf {
            for ((slot, pattern) in patterns.withIndex()) {
                if (pattern == null) continue

                +compoundOf {
                    "Item" to Registries.ITEM.getId(pattern.item).toString()
                    "Slot" to slot
                    "Tag" tryTo pattern.nbt
                }
            }
        }
    }

    companion object {
        const val MAX_PATTERN_COUNT = 12

        fun fromNbt(nbt: NbtCompound): ItemFilter {
            val ignoresNbt = NbtReader.newVar<Boolean>()
            val isBlocklistMode = NbtReader.newVar<Boolean>()
            val patterns = NbtReader.newVarBy<NbtElement, _> { it as NbtList }
            NbtReader.scanCompound(nbt) {
                "IgnoresNbt" to ignoresNbt
                "IsBlocklistMode" to isBlocklistMode
                "Patterns" to patterns
            }
            val result = ItemFilter(ignoresNbt.unwrap(), isBlocklistMode.unwrap())

            NbtReader.forEach(patterns.unwrap()) {
                val item = NbtReader.newVarBy<String, _> { s -> Registries.ITEM.get(Identifier(s)) }
                val tag = NbtReader.newVarBy<NbtElement, _> { n -> n as NbtCompound }
                val slot = NbtReader.newVar<Int>()
                NbtReader.scanCompound(it as NbtCompound) {
                    "Item" to item
                    "Slot" to slot
                    "Tag" tryTo tag
                }
                result[slot.unwrap()] = Pattern(item.unwrap(), tag.tryUnwrap())
            }
            return result
        }
    }

    data class Pattern(var item: Item, var nbt: NbtCompound?)
}