package moe.fal.naive_backpacks.upgrade

import net.minecraft.util.Identifier

interface ItemFilterOwner {
    val filterId: Identifier
}