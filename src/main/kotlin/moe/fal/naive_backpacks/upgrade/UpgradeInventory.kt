package moe.fal.naive_backpacks.upgrade

import moe.fal.naive_backpacks.backpack.BackpackDataManager
import moe.fal.naive_backpacks.utils.InventoryImpl
import net.minecraft.item.ItemStack
import net.minecraft.util.collection.DefaultedList

open class UpgradeInventory : InventoryImpl {
    private val state: BackpackDataManager.State?

    var stackMultiplier: Int = 1
        private set
    var isAutoCollectingEnabled: Boolean = false
        private set

    constructor(inner: DefaultedList<ItemStack>, storage: BackpackDataManager.State?) : super(inner) {
        this.state = storage
        updateProperties()
    }

    constructor(size: Int, storage: BackpackDataManager.State?) : super(size) {
        this.state = storage
        updateProperties()
    }

    override fun markDirty() {
        updateProperties()
        state?.markDirty()
    }

    override fun getMaxCountPerStack() = 1

    private fun updateProperties(exceptUpgrade: ItemStack? = null) {
        val multiplier = asList
            .filter { stack -> exceptUpgrade?.let { it !== stack } ?: true }
            .map { it.item }
            .filterIsInstance<StackMultiplierUpgrade>()
            .sumOf { it.multiplier }
        stackMultiplier = if (multiplier == 0) 1 else multiplier

        isAutoCollectingEnabled = asList.any { it.item is AutoCollectingUpgrade }
    }

    fun getStackMultiplierIfRemove(upgrade: ItemStack): Int {
        val result = asList
            .filter { stack -> upgrade !== stack }
            .map { it.item }
            .filterIsInstance<StackMultiplierUpgrade>()
            .sumOf { it.multiplier }
        return if (result == 0) 1 else result
    }
}