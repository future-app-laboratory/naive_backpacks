package moe.fal.naive_backpacks.block

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.backpack.BackpackKind
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.util.Identifier

object ModBlocks {
    val BACKPACKS = mapOf(
        "small_backpack" to Backpack(BackpackKind.SMALL),
        "medium_backpack" to Backpack(BackpackKind.MEDIUM),
        "large_backpack" to Backpack(BackpackKind.LARGE),
        "extra_large_backpack" to Backpack(BackpackKind.EXTRA_LARGE),
    )

    fun register() {
        for ((id, block) in BACKPACKS) {
            Registry.register(Registries.BLOCK, Identifier(MOD_ID, id), block)
        }
    }
}