package moe.fal.naive_backpacks.block

import moe.fal.naive_backpacks.ModSounds
import moe.fal.naive_backpacks.backpack.BackpackKind
import moe.fal.naive_backpacks.block.entity.BackpackBlockEntity
import moe.fal.naive_backpacks.screen.BackpackScreenHandlerFactory
import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.*
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemPlacementContext
import net.minecraft.nbt.NbtCompound
import net.minecraft.sound.BlockSoundGroup
import net.minecraft.sound.SoundCategory
import net.minecraft.state.StateManager
import net.minecraft.state.property.Properties
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.world.BlockView
import net.minecraft.world.World
import net.minecraft.world.explosion.Explosion


fun backpackSetting(): FabricBlockSettings = FabricBlockSettings
    .create()
    .strength(0.8f)


class Backpack(val kind: BackpackKind) : Block(backpackSetting()), BlockEntityProvider {
    companion object {
        private val FACING = Properties.HORIZONTAL_FACING
        private val SHAPE = createCuboidShape(2.0, 0.0, 2.0, 14.0, 15.0, 14.0)
    }

    override fun shouldDropItemsOnExplosion(explosion: Explosion?) = true

    override fun createBlockEntity(pos: BlockPos, state: BlockState): BlockEntity {
        return BackpackBlockEntity(pos, state)
    }

    override fun getSoundGroup(state: BlockState?): BlockSoundGroup {
        return BlockSoundGroup.WOOL
    }

    override fun getOutlineShape(
        state: BlockState?,
        world: BlockView?,
        pos: BlockPos?,
        context: ShapeContext?
    ): VoxelShape {
        return SHAPE
    }

    override fun appendProperties(builder: StateManager.Builder<Block, BlockState>?) {
        builder?.add(FACING)
    }


    override fun getPlacementState(ctx: ItemPlacementContext?): BlockState? {
        return defaultState.with(AbstractFurnaceBlock.FACING, ctx?.horizontalPlayerFacing?.opposite)
    }

    override fun onBreak(world: World, pos: BlockPos, state: BlockState, player: PlayerEntity) {
        val blockEntity = world.getBlockEntity(pos)
        if (blockEntity !is BackpackBlockEntity) return
        if (!world.isClient && player.isCreative) {
            val itemStack = this.asItem().defaultStack
            NbtBuilder.merge(itemStack.nbt!!) {
                BlockItem.BLOCK_ENTITY_TAG_KEY to NbtCompound().also { blockEntity.writeNbt(it) }
            }

            val itemEntity = ItemEntity(
                world,
                pos.x.toDouble() + 0.5,
                pos.y.toDouble() + 0.5,
                pos.z.toDouble() + 0.5,
                itemStack
            )
            itemEntity.setToDefaultPickupDelay()
            world.spawnEntity(itemEntity)
        }
        super.onBreak(world, pos, state, player)
    }

    override fun onUse(
        state: BlockState?,
        world: World,
        pos: BlockPos?,
        player: PlayerEntity,
        hand: Hand?,
        hit: BlockHitResult?
    ): ActionResult {
        val blockEntity = world.getBlockEntity(pos) as BackpackBlockEntity

        if (player.isSneaking && player.inventory.mainHandStack.isEmpty) {
            if (world.isClient) return ActionResult.FAIL
            val itemStack = this.asItem().defaultStack
            NbtBuilder.merge(itemStack.nbt!!) {
                BlockItem.BLOCK_ENTITY_TAG_KEY to NbtCompound().also { blockEntity.writeNbt(it) }
            }
            player.playSound(ModSounds.BACKPACK_TAKE, SoundCategory.PLAYERS, 1.0f, 1.0f)
            world.removeBlock(pos, false)
            player.giveItemStack(itemStack)
        } else {
            player.openHandledScreen(BackpackScreenHandlerFactory(blockEntity))
        }

        return ActionResult.SUCCESS
    }
}