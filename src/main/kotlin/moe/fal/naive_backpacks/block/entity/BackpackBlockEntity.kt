package moe.fal.naive_backpacks.block.entity

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.backpack.AbstractBackpack
import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.nbt.NbtCompound
import net.minecraft.network.listener.ClientPlayPacketListener
import net.minecraft.network.packet.Packet
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket
import net.minecraft.text.Text
import net.minecraft.util.Nameable
import net.minecraft.util.math.BlockPos
import java.util.*
import moe.fal.naive_backpacks.block.Backpack as BackpackBlock


open class BackpackBlockEntity(pos: BlockPos, state: BlockState) : BlockEntity(ModBlockEntities.BACKPACK, pos, state),
    AbstractBackpack, Nameable {

    @get:JvmName("getBlockEntityCustomName")
    var customName: Text? = null

    override var uuid: UUID = UUID.randomUUID()
    override val kind = (cachedState.block as BackpackBlock).kind

    public override fun writeNbt(nbt: NbtCompound) {
        val customNameStr = customName?.let { Text.Serializer.toJson(it) }
        NbtBuilder.merge(nbt) {
            "CustomName" tryTo customNameStr
            "Backpack" to compoundOf {
                "UUID" to uuid
            }
        }
    }

    override fun readNbt(nbt: NbtCompound) {
        val uuidVar = NbtReader.newVar<UUID>()
        val customNameVar = NbtReader.newVarBy<String, _>(Text.Serializer::fromJson)

        NbtReader.scanCompound(nbt) {
            "CustomName" tryTo customNameVar
            "Backpack" to compoundOf {
                "UUID" to uuidVar
            }
        }
        customName = customNameVar.tryUnwrap()
        uuid = uuidVar.unwrap()
    }

    override fun toUpdatePacket(): Packet<ClientPlayPacketListener>? {
        return BlockEntityUpdateS2CPacket.create(this)
    }

    override fun toInitialChunkDataNbt(): NbtCompound {
        return createNbt()
    }

    override fun getName(): Text = customName ?: Text.translatable("blockEntity.$MOD_ID.backpack")
    override fun getCustomName() = customName
    override fun hasCustomName() = customName != null
}