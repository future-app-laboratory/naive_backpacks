package moe.fal.naive_backpacks.block.entity

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.block.ModBlocks
import net.fabricmc.fabric.api.`object`.builder.v1.block.entity.FabricBlockEntityTypeBuilder
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.util.Identifier


object ModBlockEntities {
    val BACKPACK: BlockEntityType<BackpackBlockEntity> = Registry.register(
        Registries.BLOCK_ENTITY_TYPE,
        Identifier(MOD_ID, "backpack"),
        FabricBlockEntityTypeBuilder.create(
            { x, y -> BackpackBlockEntity(x, y) },
            *ModBlocks.BACKPACKS.values.toTypedArray()
        ).build()
    )


    fun register() {}
}