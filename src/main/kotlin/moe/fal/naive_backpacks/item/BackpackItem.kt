package moe.fal.naive_backpacks.item

import moe.fal.naive_backpacks.backpack.AbstractBackpack
import moe.fal.naive_backpacks.backpack.BackpackDataManager
import moe.fal.naive_backpacks.backpack.ItemStackBackpack
import moe.fal.naive_backpacks.block.entity.BackpackBlockEntity
import moe.fal.naive_backpacks.screen.BackpackScreenHandlerFactory
import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.block.BlockState
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.*
import net.minecraft.sound.SoundEvent
import net.minecraft.sound.SoundEvents
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.TypedActionResult
import net.minecraft.world.World
import java.util.*
import moe.fal.naive_backpacks.block.Backpack as BackpackBlock

open class BackpackItem(val block: BackpackBlock) :
    BlockItem(block, FabricItemSettings().maxCount(1)) {
    override fun use(world: World?, user: PlayerEntity?, hand: Hand?): TypedActionResult<ItemStack> {
        if (user == null || hand == null) {
            return TypedActionResult.fail(ItemStack.EMPTY)
        }
        val item = user.handItems!!.drop(if (hand == Hand.MAIN_HAND) 0 else 1)[0]
        val backpack = ItemStackBackpack(item)
        user.openHandledScreen(BackpackScreenHandlerFactory(backpack))
        return TypedActionResult.success(item)
    }

    override fun useOnBlock(context: ItemUsageContext?): ActionResult {
        if (context == null) {
            return ActionResult.FAIL
        }
        val player = context.player ?: return ActionResult.FAIL
        if (player.isSneaking) {
            val result = place(ItemPlacementContext(context))
            return if (result.isAccepted) ActionResult.SUCCESS else ActionResult.FAIL
        }

        val result = use(context.world, context.player, context.hand)
        return if (result.result.isAccepted) ActionResult.SUCCESS else ActionResult.FAIL
    }

    override fun place(context: ItemPlacementContext): ActionResult {
        val customName = context.stack.run {
            if (hasCustomName()) name else null
        }
        return super.place(context).apply {
            if (customName != null && isAccepted) {
                val be = context.world.getBlockEntity(context.blockPos)!!
                if (be is BackpackBlockEntity) {
                    be.customName = customName
                }
            }
        }
    }

    override fun getPlaceSound(state: BlockState?): SoundEvent {
        return SoundEvents.BLOCK_WOOL_PLACE
    }

    override fun getDefaultStack(): ItemStack =
        super.getDefaultStack().apply {
            NbtBuilder.merge(orCreateNbt) {
                BLOCK_ENTITY_TAG_KEY to AbstractBackpack.defaultNbt()
            }
        }


    override fun onItemEntityDestroyed(entity: ItemEntity) {
        super.onItemEntityDestroyed(entity)
        if (entity.stack.hasNbt()) {
            val uuid: UUID = NbtReader.query(getBlockEntityNbt(entity.stack)!!).let {
                it["Backpack"]["UUID"].asUuid
            }

            if (entity.world.isClient) return
            val spawn = { itemStack: ItemStack ->
                entity.world.spawnEntity(
                    ItemEntity(entity.world, entity.x, entity.y, entity.z, itemStack)
                )
            }
            BackpackDataManager(entity.server!!).runAndRemove(uuid) {
                for (item in content.asList) {
                    while (item.count > item.maxCount) spawn(item.split(item.maxCount))
                    spawn(item)
                }
                ItemUsage.spawnItemContents(entity, upgrades.asList.stream())
                content.clear()
                upgrades.clear()
            }
        }
    }
}
