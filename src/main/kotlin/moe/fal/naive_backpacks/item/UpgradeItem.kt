package moe.fal.naive_backpacks.item

import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.minecraft.item.Item


open class UpgradeItem(tags: Iterable<String>) : Item(FabricItemSettings().maxCount(1)) {
    private val tags = tags.toSet()

    constructor(vararg tags: String) : this(tags.asIterable())

    fun conflictsWith(upgrades: Iterable<UpgradeItem>) = upgrades.any { upgrade ->
        tags.any { upgrade.tags.contains(it) }
    }
}