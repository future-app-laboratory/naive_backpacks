package moe.fal.naive_backpacks.item

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.block.ModBlocks
import moe.fal.naive_backpacks.upgrade.AutoCollectingUpgrade
import moe.fal.naive_backpacks.upgrade.StackMultiplierUpgrade
import net.fabricmc.fabric.api.item.v1.FabricItemSettings
import net.fabricmc.fabric.api.itemgroup.v1.FabricItemGroup
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.registry.RegistryKey
import net.minecraft.registry.RegistryKeys
import net.minecraft.text.Text
import net.minecraft.util.Identifier

object ModItems {
    private val BACKPACKS = ModBlocks.BACKPACKS.entries.associate {
        it.key to BackpackItem(it.value)
    }

    private val MATERIALS = listOf(
        "rope",
        "strap",
        "cloth",
        "advanced_cloth",
        "zipper",
        "advanced_zipper",
        "small_bag",
        "large_bag",
        "upgrade_slots",
        "advanced_upgrade_slots",
        "upgrade_template",
        "advanced_upgrade_template",
    ).associateWith { Item(FabricItemSettings()) }

    private val UPGRADES = mapOf(
        "crafting_table_upgrade" to UpgradeItem(),
        "compressor_upgrade" to UpgradeItem(),
        "stonecutter_upgrade" to UpgradeItem(),

        "furnace_upgrade" to UpgradeItem("FURNACE"),
        "advanced_furnace_upgrade" to UpgradeItem("FURNACE"),

        "trash_bin_upgrade" to UpgradeItem("TRASH_BIN"),
        "advanced_trash_bin_upgrade" to UpgradeItem("TRASH_BIN"),

        "stack_x2_upgrade" to object : StackMultiplierUpgrade, UpgradeItem() {
            override val multiplier = 2
        },
        "stack_x4_upgrade" to object : StackMultiplierUpgrade, UpgradeItem() {
            override val multiplier = 4
        },
        "stack_x8_upgrade" to object : StackMultiplierUpgrade, UpgradeItem() {
            override val multiplier = 8
        },
        "stack_x16_upgrade" to object : StackMultiplierUpgrade, UpgradeItem() {
            override val multiplier = 16
        },

        "fluid_storage_upgrade" to UpgradeItem("FLUID"),
        "advanced_fluid_storage_upgrade" to UpgradeItem("FLUID"),
        "exp_storage_upgrade" to UpgradeItem("FLUID"),
        "advanced_exp_storage_upgrade" to UpgradeItem("FLUID"),

        "energy_storage_upgrade" to UpgradeItem("ENERGY"),
        "advanced_energy_storage_upgrade" to UpgradeItem("ENERGY"),

        "collecting_upgrade" to object : AutoCollectingUpgrade, UpgradeItem("COLLECTING") {},
        "advanced_collecting_upgrade" to object : AutoCollectingUpgrade, UpgradeItem("COLLECTING") {},
        "magnet_upgrade" to object : AutoCollectingUpgrade, UpgradeItem("COLLECTING") {},
        "advanced_magnet_upgrade" to object : AutoCollectingUpgrade, UpgradeItem("COLLECTING") {},

        "feeding_upgrade" to UpgradeItem("FEEDING"),
        "advanced_feeding_upgrade" to UpgradeItem("FEEDING"),

        "restocking_upgrade" to UpgradeItem("RESTOCKING"),
        "advanced_restocking_upgrade" to UpgradeItem("RESTOCKING"),
    )

    private val ITEM_TABLES = listOf(BACKPACKS, MATERIALS, UPGRADES)

    fun register() {
        for (items in ITEM_TABLES) {
            items.forEach { Registry.register(Registries.ITEM, Identifier(MOD_ID, it.key), it.value) }
        }

        Registry.register(
            Registries.ITEM_GROUP,
            RegistryKey.of(RegistryKeys.ITEM_GROUP, Identifier(MOD_ID, "main")),
            FabricItemGroup.builder()
                .displayName(Text.translatable("itemGroup.$MOD_ID.main"))
                .icon { ItemStack(BACKPACKS["small_backpack"]) }
                .entries { _, entries ->
                    for (items in ITEM_TABLES) {
                        items.values.forEach { entries.add(ItemStack(it)) }
                    }
                }
                .build()
        )
    }
}