package moe.fal.naive_backpacks.utils.nbt

import moe.fal.naive_backpacks.utils.nbt.ItemStackUtils.toNbt
import net.minecraft.item.ItemStack
import net.minecraft.nbt.*
import net.minecraft.util.collection.DefaultedList
import java.util.*

@Suppress("unused")
object NbtBuilder {
    class CompoundFactory(val inner: CompoundBuilder.() -> Unit)
    class ListFactory(val inner: ListBuilder.() -> Unit)
    class Number<T>(val inner: T)

    fun compoundOf(f: CompoundBuilder.() -> Unit) = CompoundBuilder().apply(f).nbt

    fun listOf(f: ListBuilder.() -> Unit) = ListBuilder().apply(f).nbt

    fun merge(compound: NbtCompound, f: CompoundBuilder.() -> Unit) {
        CompoundBuilder(compound).apply(f).nbt
    }

    fun append(list: NbtList, f: ListBuilder.() -> Unit) {
        ListBuilder(list).apply(f).nbt
    }


    class CompoundBuilder(val nbt: NbtCompound) {
        constructor() : this(NbtCompound())

        fun compoundOf(f: CompoundBuilder.() -> Unit) = CompoundFactory(f)
        fun listOf(f: ListBuilder.() -> Unit) = ListFactory(f)

        infix fun String.tryTo(value: String?) = value?.let { nbt.putString(this, it) }
        infix fun String.tryTo(value: UUID?) = value?.let { nbt.putUuid(this, it) }
        infix fun String.tryTo(value: Short?) = value?.let { nbt.putShort(this, it) }
        infix fun String.tryTo(value: Long?) = value?.let { nbt.putLong(this, it) }
        infix fun String.tryTo(value: Float?) = value?.let { nbt.putFloat(this, it) }
        infix fun String.tryTo(value: Int?) = value?.let { nbt.putInt(this, it) }
        infix fun String.tryTo(value: Double?) = value?.let { nbt.putDouble(this, it) }
        infix fun String.tryTo(value: Byte?) = value?.let { nbt.putByte(this, it) }
        infix fun String.tryTo(value: Boolean?) = value?.let { nbt.putBoolean(this, it) }
        infix fun <T : NbtElement> String.tryTo(value: T?) = value?.let { nbt.put(this, it) }

        infix fun String.tryTo(value: DefaultedList<ItemStack>?) = value?.let {
            nbt.put(this, it.toNbt())
        }

        infix fun String.tryTo(c: CompoundFactory?) = c?.let {
            nbt.put(this, CompoundBuilder().apply(it.inner).nbt)
        }

        infix fun String.tryTo(l: ListFactory?) = l?.let {
            nbt.put(this, ListBuilder().apply(it.inner).nbt)
        }

        infix fun String.to(value: String) = nbt.putString(this, value)
        infix fun String.to(value: UUID) = nbt.putUuid(this, value)
        infix fun String.to(value: Short) = nbt.putShort(this, value)
        infix fun String.to(value: Long) = nbt.putLong(this, value)
        infix fun String.to(value: Float) = nbt.putFloat(this, value)
        infix fun String.to(value: Int) = nbt.putInt(this, value)
        infix fun String.to(value: Double) = nbt.putDouble(this, value)
        infix fun String.to(value: Byte) = nbt.putByte(this, value)
        infix fun String.to(value: Boolean) = nbt.putBoolean(this, value)
        infix fun <T : NbtElement> String.to(value: T) = nbt.put(this, value)

        infix fun String.to(value: DefaultedList<ItemStack>) {
            nbt.put(this, value.toNbt())
        }

        infix fun String.to(c: CompoundFactory) {
            nbt.put(this, CompoundBuilder().apply(c.inner).nbt)
        }

        infix fun String.to(l: ListFactory) {
            nbt.put(this, ListBuilder().apply(l.inner).nbt)
        }
    }

    class ListBuilder(val nbt: NbtList) {
        constructor() : this(NbtList())

        fun compoundOf(f: CompoundBuilder.() -> Unit) = CompoundFactory(f)
        fun listOf(f: ListBuilder.() -> Unit) = ListFactory(f)
        fun <T> number(n: T) = Number(n)

        @JvmName("withInt")
        operator fun Number<Int>.unaryPlus() = nbt.add(NbtInt.of(inner))

        @JvmName("withShort")
        operator fun Number<Short>.unaryPlus() = nbt.add(NbtShort.of(inner))

        @JvmName("withLong")
        operator fun Number<Long>.unaryPlus() = nbt.add(NbtLong.of(inner))

        @JvmName("withByte")
        operator fun Number<Byte>.unaryPlus() = nbt.add(NbtByte.of(inner))

        @JvmName("withFloat")
        operator fun Number<Float>.unaryPlus() = nbt.add(NbtFloat.of(inner))

        @JvmName("withDouble")
        operator fun Number<Double>.unaryPlus() = nbt.add(NbtDouble.of(inner))

        @JvmName("withDefaultListOfItemStack")
        operator fun DefaultedList<ItemStack>.unaryPlus() {
            nbt.add(this.toNbt())
        }

        operator fun String.unaryPlus() = nbt.add(NbtString.of(this))
        operator fun UUID.unaryPlus() = nbt.add(NbtHelper.fromUuid(this))
        operator fun NbtElement.unaryPlus() = nbt.add(this)
        operator fun Boolean.unaryPlus() = nbt.add(NbtByte.of(this))
        operator fun CompoundFactory.unaryPlus() = nbt.add(CompoundBuilder().apply(inner).nbt)
        operator fun ListFactory.unaryPlus() = nbt.add(ListBuilder().apply(inner).nbt)
    }
}


