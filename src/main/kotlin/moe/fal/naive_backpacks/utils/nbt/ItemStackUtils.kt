package moe.fal.naive_backpacks.utils.nbt

import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtList
import net.minecraft.util.collection.DefaultedList

object ItemStackUtils {
    private fun fromNbt(nbt: NbtCompound): ItemStack {
        val count = NbtReader.query(nbt)["Count"].asInt
        NbtBuilder.merge(nbt) {
            "Count" to 1.toByte()
        }
        return ItemStack.fromNbt(nbt).apply {
            setCount(count)
        }
    }

    private fun writeNbt(stack: ItemStack, nbt: NbtCompound) {
        stack.writeNbt(nbt)
        NbtBuilder.merge(nbt) {
            "Count" to stack.count
        }
    }

    fun DefaultedList<ItemStack>.readFromNbt(nbtList: NbtList) {
        for (i in nbtList.indices) {
            val nbtCompound = nbtList.getCompound(i)
            val j = nbtCompound.getByte("Slot").toInt() and 0xFF
            if (j >= this.size) continue
            this[j] = fromNbt(nbtCompound)
        }
    }

    fun DefaultedList<ItemStack>.toNbt(): NbtList {
        val nbtList = NbtList()
        for (i in this.indices) {
            val itemStack = this[i]
            if (itemStack.isEmpty) continue
            val nbtCompound = NbtCompound()
            nbtCompound.putByte("Slot", i.toByte())
            writeNbt(itemStack, nbtCompound)
            nbtList.add(nbtCompound)
        }
        return nbtList
    }
}