package moe.fal.naive_backpacks.utils.nbt

import moe.fal.naive_backpacks.utils.nbt.ItemStackUtils.readFromNbt
import net.minecraft.item.ItemStack
import net.minecraft.nbt.*
import net.minecraft.util.collection.DefaultedList
import java.util.*

@Suppress("unused")
object NbtReader {
    interface Var<T> {
        fun set(t: T)
    }

    class NewVar<T> : Var<T> {
        private var inner: T? = null
        override fun set(t: T) {
            inner = t
        }

        fun tryUnwrap(): T? = inner
        fun unwrap(): T = inner!!
    }

    class NewVarBy<T, U>(private val f: (T) -> U) : Var<T> {
        private var inner: U? = null
        override fun set(t: T) {
            inner = f(t)
        }

        fun tryUnwrap(): U? = inner
        fun unwrap(): U = inner!!
    }

    fun <T> newVar() = NewVar<T>()

    fun <T, U> newVarBy(f: (T) -> U) = NewVarBy(f)

    fun query(nbt: NbtElement) = NbtQuery(nbt)

    fun forEach(nbt: NbtList, f: (NbtElement) -> Unit) {
        for (i in (0..<nbt.size)) {
            f(nbt[i])
        }
    }

    fun scanCompound(nbt: NbtCompound, f: CompoundScanner.() -> Unit) {
        CompoundScanner(nbt).apply(f)
    }

    class NbtQuery(internal val nbt: NbtElement) {
        operator fun get(index: Int): NbtQuery {
            val result = if (nbt is NbtList) {
                nbt[index]?.let { NbtQuery(it) }
            } else null
            return result ?: throw IllegalArgumentException("Unexpected Non-List NBT Element: $nbt")
        }

        operator fun get(key: String): NbtQuery {
            val result = if (nbt is NbtCompound) {
                nbt.get(key)?.let { NbtQuery(it) }
            } else null
            return result ?: throw IllegalArgumentException("Unexpected Non-Compound NBT Element: $nbt")
        }

        fun contains(key: String): Boolean = asCompound.contains(key)

        val asUuid: UUID
            get() = NbtHelper.toUuid(nbt)

        val asString: String
            get() = if (nbt is NbtString) nbt.asString() else throw IllegalArgumentException("Unexpected Non-String NBT Element: $nbt")

        val asCompound: NbtCompound
            get() = if (nbt is NbtCompound) nbt else throw IllegalArgumentException("Unexpected Non-Compound NBT Element: $nbt")

        val asList: NbtList
            get() = if (nbt is NbtList) nbt else throw IllegalArgumentException("Unexpected Non-List NBT Element: $nbt")

        val asBoolean: Boolean
            get() = if (nbt is AbstractNbtNumber) {
                nbt.byteValue().toInt() != 0
            } else throw IllegalArgumentException("Unexpected Non-Boolean NBT Element: $nbt")

        val asInt: Int
            get() = if (nbt is AbstractNbtNumber) {
                nbt.intValue()
            } else throw IllegalArgumentException("Unexpected Non-Int NBT Element: $nbt")

        val asShort: Short
            get() = if (nbt is AbstractNbtNumber) {
                nbt.shortValue()
            } else throw IllegalArgumentException("Unexpected Non-Short NBT Element: $nbt")

        val asLong: Long
            get() = if (nbt is AbstractNbtNumber) {
                nbt.longValue()
            } else throw IllegalArgumentException("Unexpected Non-Long NBT Element: $nbt")

        val asByte: Byte
            get() = if (nbt is AbstractNbtNumber) {
                nbt.byteValue()
            } else throw IllegalArgumentException("Unexpected Non-Byte NBT Element: $nbt")

        val asFloat: Float
            get() = if (nbt is AbstractNbtNumber) {
                nbt.floatValue()
            } else throw IllegalArgumentException("Unexpected Non-Float NBT Element: $nbt")

        val asDouble: Double
            get() = if (nbt is AbstractNbtNumber) {
                nbt.doubleValue()
            } else throw IllegalArgumentException("Unexpected Non-Double NBT Element: $nbt")

    }

    class CompoundScanner(nbt: NbtCompound) {
        class Compound(internal val f: CompoundScanner.() -> Unit)

        val nbt = NbtQuery(nbt)


        fun compoundOf(f: CompoundScanner.() -> Unit) = Compound(f)
//        fun list(f: NbtBuilder.ListBuilder.() -> Unit) = NbtBuilder.ListFactory(f)

        @JvmName("toVarString")
        infix fun String.to(`var`: Var<String>) = `var`.set(nbt[this].asString)

        @JvmName("toVarUUID")
        infix fun String.to(`var`: Var<UUID>) = `var`.set(nbt[this].asUuid)

        @JvmName("toVarShort")
        infix fun String.to(`var`: Var<Short>) = `var`.set(nbt[this].asShort)

        @JvmName("toVarLong")
        infix fun String.to(`var`: Var<Long>) = `var`.set(nbt[this].asLong)

        @JvmName("toVarFloat")
        infix fun String.to(`var`: Var<Float>) = `var`.set(nbt[this].asFloat)

        @JvmName("toVarInt")
        infix fun String.to(`var`: Var<Int>) = `var`.set(nbt[this].asInt)

        @JvmName("toVarDouble")
        infix fun String.to(`var`: Var<Double>) = `var`.set(nbt[this].asDouble)

        @JvmName("toVarByte")
        infix fun String.to(`var`: Var<Byte>) = `var`.set(nbt[this].asByte)

        @JvmName("toVarBoolean")
        infix fun String.to(`var`: Var<Boolean>) = `var`.set(nbt[this].asBoolean)

        @JvmName("toVarNbtElement")
        infix fun String.to(`var`: Var<NbtElement>) = `var`.set(nbt[this].nbt)

        infix fun String.to(list: DefaultedList<ItemStack>) {
            list.readFromNbt(nbt[this].asList)
        }

        infix fun String.to(c: Compound) {
            CompoundScanner(nbt[this].asCompound).apply(c.f)
        }

        @JvmName("tryToVarString")
        infix fun String.tryTo(`var`: Var<String>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asString)
            }
        }

        @JvmName("tryToVarByte")
        infix fun String.tryTo(`var`: Var<Byte>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asByte)
            }
        }

        @JvmName("tryToVarShort")
        infix fun String.tryTo(`var`: Var<Short>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asShort)
            }
        }

        @JvmName("tryToVarInt")
        infix fun String.tryTo(`var`: Var<Int>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asInt)
            }
        }

        @JvmName("tryToVarLong")
        infix fun String.tryTo(`var`: Var<Long>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asLong)
            }
        }

        @JvmName("tryToVarBoolean")
        infix fun String.tryTo(`var`: Var<Boolean>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asBoolean)
            }
        }

        @JvmName("tryToVarFloat")
        infix fun String.tryTo(`var`: Var<Float>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asFloat)
            }
        }

        @JvmName("tryToVarDouble")
        infix fun String.tryTo(`var`: Var<Double>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].asDouble)
            }
        }

        @JvmName("tryToVarNbtElement")
        infix fun String.tryTo(`var`: Var<NbtElement>) {
            if (nbt.asCompound.contains(this)) {
                `var`.set(nbt[this].nbt)
            }
        }

        infix fun String.tryTo(c: Compound) {
            if (nbt.asCompound.contains(this)) {
                CompoundScanner(nbt[this].asCompound).apply(c.f)
            }
        }
    }
}