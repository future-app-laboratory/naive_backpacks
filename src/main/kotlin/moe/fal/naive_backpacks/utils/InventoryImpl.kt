package moe.fal.naive_backpacks.utils

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventories
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.util.collection.DefaultedList

open class InventoryImpl(private val inner: DefaultedList<ItemStack>) : Inventory {
    constructor(size: Int) : this(DefaultedList.ofSize(size, ItemStack.EMPTY))

    val asList
        get() = inner

    override fun clear() {
        inner.clear()
    }

    override fun size() = inner.size

    override fun isEmpty() = inner.isEmpty()

    override fun getStack(slot: Int) = inner[slot]

    override fun removeStack(slot: Int): ItemStack = Inventories.removeStack(inner, slot)

    override fun canPlayerUse(player: PlayerEntity?) = true

    override fun removeStack(slot: Int, amount: Int): ItemStack = Inventories.splitStack(inner, slot, amount)

    override fun markDirty() {}

    override fun setStack(slot: Int, stack: ItemStack?) {
        inner[slot] = stack
    }
}