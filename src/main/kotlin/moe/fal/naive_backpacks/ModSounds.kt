package moe.fal.naive_backpacks

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.sound.SoundEvent
import net.minecraft.util.Identifier

object ModSounds {
    val BACKPACK_OPEN: SoundEvent = SoundEvent.of(Identifier(MOD_ID, "backpack.open"))
    val BACKPACK_CLOSE: SoundEvent = SoundEvent.of(Identifier(MOD_ID, "backpack.close"))
    val BACKPACK_TAKE: SoundEvent = SoundEvent.of(Identifier(MOD_ID, "backpack.take"))

    fun register() {
        Registry.register(Registries.SOUND_EVENT, BACKPACK_OPEN.id, BACKPACK_OPEN)
        Registry.register(Registries.SOUND_EVENT, BACKPACK_CLOSE.id, BACKPACK_CLOSE)
        Registry.register(Registries.SOUND_EVENT, BACKPACK_TAKE.id, BACKPACK_TAKE)
    }
}