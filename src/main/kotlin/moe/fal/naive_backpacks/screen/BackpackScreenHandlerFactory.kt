package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.ModSounds
import moe.fal.naive_backpacks.backpack.AbstractBackpack
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.nbt.NbtCompound
import net.minecraft.network.PacketByteBuf
import net.minecraft.screen.ScreenHandler
import net.minecraft.server.network.ServerPlayerEntity
import net.minecraft.sound.SoundCategory
import net.minecraft.text.Text

class BackpackScreenHandlerFactory(private val backpack: AbstractBackpack) : ExtendedScreenHandlerFactory {
    override fun getDisplayName(): Text = backpack.name

    override fun writeScreenOpeningData(player: ServerPlayerEntity?, buf: PacketByteBuf) {
        buf.writeNbt(NbtCompound().apply {
            putString("kind", backpack.kind.toString())
        })
    }

    override fun createMenu(
        syncId: Int,
        playerInventory: PlayerInventory,
        player: PlayerEntity,
    ): ScreenHandler {
        player.playSound(ModSounds.BACKPACK_OPEN, SoundCategory.PLAYERS, 1.0f, 1.0f)
        return BackpackScreenHandler(syncId, playerInventory, backpack)
    }
}