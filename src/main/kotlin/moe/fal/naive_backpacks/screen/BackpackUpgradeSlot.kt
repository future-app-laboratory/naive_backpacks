package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.backpack.BackpackDataManager
import moe.fal.naive_backpacks.item.UpgradeItem
import moe.fal.naive_backpacks.upgrade.StackMultiplierUpgrade
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.screen.slot.Slot

class BackpackUpgradeSlot(
    private val state: BackpackDataManager.State,
    inventory: Inventory,
    index: Int,
    x: Int,
    y: Int
) : Slot(inventory, index, x, y) {
    override fun canInsert(stack: ItemStack): Boolean {
        val item = stack.item
        return item is UpgradeItem && !item.conflictsWith(state.upgrades.asList
            .filter { !it.isEmpty }
            .map { it.item as UpgradeItem })
    }

    override fun canTakeItems(playerEntity: PlayerEntity): Boolean {
        if (stack.item is StackMultiplierUpgrade) {
            val multiplier = state.upgrades.getStackMultiplierIfRemove(stack)
            return state.content.asList.all { it.count <= it.maxCount * multiplier }
        }

        return true
    }
}