package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.ModSounds
import moe.fal.naive_backpacks.backpack.AbstractBackpack
import moe.fal.naive_backpacks.backpack.BackpackKind
import moe.fal.naive_backpacks.backpack.BackpackDataManager
import moe.fal.naive_backpacks.block.entity.BackpackBlockEntity
import moe.fal.naive_backpacks.item.BackpackItem
import moe.fal.naive_backpacks.backpack.ItemStackBackpack
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.inventory.Inventory
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.network.PacketByteBuf
import net.minecraft.screen.ScreenHandler
import net.minecraft.screen.slot.Slot
import net.minecraft.sound.SoundCategory

class BackpackScreenHandler : ScreenHandler {
    val backpack: AbstractBackpack
    private val playerInventory: PlayerInventory
    private lateinit var state: BackpackDataManager.State

    constructor(
        syncId: Int,
        playerInventory: PlayerInventory,
        backpack: AbstractBackpack,
    ) : super(ModScreenHandlers.BACKPACK_SCREEN_HANDLER_TYPE, syncId) {
        this.playerInventory = playerInventory
        this.backpack = backpack

        initSlots()
    }

    constructor(
        syncId: Int,
        playerInventory: PlayerInventory,
        buf: PacketByteBuf
    ) : super(ModScreenHandlers.BACKPACK_SCREEN_HANDLER_TYPE, syncId) {
        this.playerInventory = playerInventory
        val clientInfo = buf.readNbt()
        backpack = AbstractBackpack.fromKind(BackpackKind.valueOf(clientInfo!!.getString("kind")!!))

        initSlots()
    }

    private fun initSlots() {
        val server = playerInventory.player.server
        state = if (server != null) {
            BackpackDataManager(server).getOrCreate(backpack)
        } else BackpackDataManager.State.fromBackpack(backpack)

        state.content.onOpen(playerInventory.player)
        for (i in 0..<backpack.kind.rows()) {
            for (j in 0..8) {
                addSlot(BackpackContentSlot(state, state.content, j + i * 9, 28 + j * 18, i * 18 + 18))
            }
        }

        state.upgrades.onOpen(playerInventory.player)
        for (i in 0..<backpack.kind.maxUpgradeCount()) {
            addSlot(BackpackUpgradeSlot(state, state.upgrades, i, 3, i * 18 + 18))
        }

        var yOffset = backpack.kind.rows() * 18 + 32
        if (backpack.kind == BackpackKind.EXTRA_LARGE) {
            yOffset -= 2
        }
        for (i in 0..2) {
            for (j in 0..8) {
                addSlot(Slot(playerInventory, j + i * 9 + 9, 28 + j * 18, i * 18 + yOffset))
            }
        }
        for (i in 0..8) {
            addSlot(Slot(playerInventory, i, 28 + i * 18, 58 + yOffset))
        }
    }

    override fun dropInventory(player: PlayerEntity, inventory: Inventory) {
        super.dropInventory(player, inventory)
    }

    override fun onClosed(player: PlayerEntity) {
        state.content.onClose(player)
        player.playSound(ModSounds.BACKPACK_CLOSE, SoundCategory.PLAYERS, 1.0f, 1.0f)
        super.onClosed(player)
    }

    override fun canUse(player: PlayerEntity): Boolean {
        if (backpack is BackpackBlockEntity) {
            val be = player.world.getBlockEntity(backpack.pos) ?: return false
            return if (be is BackpackBlockEntity) {
                be.uuid == backpack.uuid && Inventory.canPlayerUse(be, player)
            } else false
        }
        if (backpack is ItemStackBackpack) {
            return player.inventory.containsAny {
                it.item is BackpackItem && it.hasNbt() && NbtReader.query(it.nbt!!).let { nbt ->
                    nbt[BlockItem.BLOCK_ENTITY_TAG_KEY]["Backpack"]["UUID"].asUuid == backpack.uuid
                }
            }
        }
        return true
    }

    override fun quickMove(player: PlayerEntity?, invSlot: Int): ItemStack {
        var newStack = ItemStack.EMPTY
        val slot = slots.getOrNull(invSlot)
        if (slot != null && slot.hasStack()) {
            val originalStack = slot.stack
            newStack = originalStack.copy()
            val upgradeInvBegin = backpack.kind.contentSize()
            val playerInvBegin = upgradeInvBegin + backpack.kind.maxUpgradeCount()

            val result = if (invSlot < playerInvBegin) {
                insertItem(originalStack, playerInvBegin, slots.size, true)
            } else {
                insertItem(originalStack, upgradeInvBegin, playerInvBegin, false) or
                        insertItem(originalStack, 0, upgradeInvBegin, false)
            }
            if (!result) {
                return ItemStack.EMPTY
            }
            if (originalStack.isEmpty) {
                slot.stack = ItemStack.EMPTY
            } else {
                slot.markDirty()
            }
        }

        return newStack
    }

    override fun insertItem(stack: ItemStack, startIndex: Int, endIndex: Int, fromLast: Boolean): Boolean {
        var itemStack: ItemStack
        var slot: Slot = slots[startIndex]
        var bl = false
        var i = startIndex
        if (fromLast) {
            i = endIndex - 1
        }

        if (with(stack) { slot.getMaxItemCount(this) > 1 && !(isDamageable && isDamaged) }) {
            while (!stack.isEmpty && if (fromLast) i >= startIndex else i < endIndex) {
                slot = slots[i]
                itemStack = slot.stack
                if (!itemStack.isEmpty && ItemStack.canCombine(stack, itemStack)) {
                    val j = itemStack.count + stack.count
                    if (j <= slot.getMaxItemCount(stack)) {
                        stack.count = 0
                        itemStack.count = j
                        slot.markDirty()
                        bl = true
                    } else if (itemStack.count < slot.getMaxItemCount(stack)) {
                        stack.decrement(slot.getMaxItemCount(stack) - itemStack.count)
                        itemStack.count = slot.getMaxItemCount(stack)
                        slot.markDirty()
                        bl = true
                    }
                }
                if (fromLast) {
                    --i
                    continue
                }
                ++i
            }
        }
        if (!stack.isEmpty) {
            i = if (fromLast) endIndex - 1 else startIndex
            while (if (fromLast) i >= startIndex else i < endIndex) {
                slot = slots[i]
                itemStack = slot.stack
                if (itemStack.isEmpty && slot.canInsert(stack)) {
                    if (stack.count > slot.getMaxItemCount(stack)) {
                        slot.stack = stack.split(slot.getMaxItemCount(stack))
                    } else {
                        slot.stack = stack.split(stack.count)
                    }
                    slot.markDirty()
                    bl = true
                    break
                }
                if (fromLast) {
                    --i
                    continue
                }
                ++i
            }
        }
        return bl
    }
}