package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.backpack.BackpackInventory
import moe.fal.naive_backpacks.backpack.BackpackDataManager
import net.minecraft.inventory.Inventory
import net.minecraft.item.ItemStack
import net.minecraft.screen.slot.Slot
import kotlin.math.min

class BackpackContentSlot(
    private val state: BackpackDataManager.State,
    inventory: Inventory,
    index: Int,
    x: Int,
    y: Int
) : Slot(inventory, index, x, y) {

    override fun canInsert(stack: ItemStack): Boolean {
        return (inventory as BackpackInventory).canInsert(index, stack)
    }

    override fun getMaxItemCount(stack: ItemStack): Int {
        return min(maxItemCount, stack.maxCount * state.upgrades.stackMultiplier)
    }
}