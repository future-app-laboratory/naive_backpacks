package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.NaiveBackpacks
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerType
import net.minecraft.registry.Registries
import net.minecraft.registry.Registry
import net.minecraft.util.Identifier

object ModScreenHandlers {
    val BACKPACK_SCREEN_HANDLER_TYPE = ExtendedScreenHandlerType { syncId, inv, buf ->
        BackpackScreenHandler(syncId, inv, buf)
    }

    fun register() {
        Registry.register(
            Registries.SCREEN_HANDLER,
            Identifier(NaiveBackpacks.MOD_ID, "backpack"),
            BACKPACK_SCREEN_HANDLER_TYPE
        )
    }
}