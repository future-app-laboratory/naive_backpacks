package moe.fal.naive_backpacks.backpack

import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import net.minecraft.nbt.NbtCompound
import net.minecraft.text.Text
import net.minecraft.util.Nameable
import java.util.*

interface AbstractBackpack : Nameable {
    val uuid: UUID
    val kind: BackpackKind

    companion object {
        fun fromKind(kind: BackpackKind): AbstractBackpack = object : AbstractBackpack {
            override val uuid: UUID
                get() = UUID.randomUUID()

            override val kind: BackpackKind
                get() = kind

            override fun getName(): Text = Text.literal("Backpack")
        }

        fun defaultNbt(): NbtCompound = NbtBuilder.compoundOf {
            "Backpack" to compoundOf {
                "UUID" to UUID.randomUUID()
            }
        }
    }
}