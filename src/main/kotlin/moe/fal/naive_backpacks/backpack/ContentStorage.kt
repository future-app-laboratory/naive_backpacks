package moe.fal.naive_backpacks.backpack

import net.fabricmc.fabric.api.transfer.v1.item.InventoryStorage
import net.fabricmc.fabric.api.transfer.v1.storage.StorageUtil
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SidedInventory
import net.minecraft.inventory.SimpleInventory
import net.minecraft.item.ItemStack
import net.minecraft.util.math.Direction


class ContentStorage(private val state: BackpackDataManager.State) {
    val slots = (0..<state.kind.contentSize()).toList().toIntArray()

    val inventory: Inventory = object: SimpleInventory(state.kind.contentSize()), SidedInventory {
        override fun markDirty() = state.markDirty()
        override fun getAvailableSlots(side: Direction?): IntArray = slots
        override fun canExtract(slot: Int, stack: ItemStack?, dir: Direction?) = true

        override fun canInsert(slot: Int, stack: ItemStack?, dir: Direction?): Boolean {
            TODO("Not yet implemented")
        }
    }

    val storage = InventoryStorage.of(inventory, null)
}