package moe.fal.naive_backpacks.backpack

import moe.fal.naive_backpacks.NaiveBackpacks
import moe.fal.naive_backpacks.NaiveBackpacks.LOGGER
import moe.fal.naive_backpacks.item.BackpackItem
import moe.fal.naive_backpacks.utils.InventoryImpl
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.util.collection.DefaultedList

open class BackpackInventory : InventoryImpl {
    private val state: BackpackDataManager.State?

    constructor(inner: DefaultedList<ItemStack>, storage: BackpackDataManager.State?) : super(inner) {
        this.state = storage
    }

    constructor(size: Int, storage: BackpackDataManager.State?) : super(size) {
        this.state = storage
    }

    override fun markDirty() {
        state?.markDirty()
    }

    fun insertItem(stack: ItemStack): Boolean {
        var oldStack: ItemStack = this.asList[0]
        var result = false

        if (with(oldStack) { getMaxCount(this) > 1 && !(isDamageable && isDamaged) }) {
            for (i in 0..<size()) {
                if (stack.isEmpty) break
                oldStack = asList[i]
                if (oldStack.isEmpty || !ItemStack.canCombine(stack, oldStack)) continue

                val sum = oldStack.count + stack.count
                if (sum <= getMaxCount(stack)) {
                    stack.count = 0
                    oldStack.count = sum
                    markDirty()
                    result = true
                } else if (oldStack.count < getMaxCount(stack)) {
                    stack.decrement(getMaxCount(stack) - oldStack.count)
                    oldStack.count = getMaxCount(stack)
                    markDirty()
                    result = true
                }
            }
        }

        if (stack.isEmpty) return result
        for (i in 0..size()) {
            oldStack = asList[i]
            if (oldStack.isEmpty && canInsert(i, stack)) {
                asList[i] = if (stack.count > getMaxCount(stack)) {
                    stack.split(getMaxCount(stack))
                } else {
                    stack.split(stack.count)
                }
                markDirty()
                return true
            }
        }

        return result
    }

    override fun getMaxCountPerStack(): Int {
        val multiplier = state?.upgrades?.stackMultiplier ?: 1
        return 64 * multiplier
    }

    private fun getMaxCount(itemStack: ItemStack): Int {
        val multiplier = state?.upgrades?.stackMultiplier ?: 1
        LOGGER.info("multiplier = $multiplier")
        return itemStack.maxCount * multiplier
    }

    fun canInsert(index: Int, stack: ItemStack): Boolean {
        val hasNestingUpgrade = false
        val isStorableItem = {
            stack.streamTags().anyMatch {
                it.id == Identifier(NaiveBackpacks.MOD_ID, "storable")
            }
        }
        val isCurrentBackpack = {
            stack.item is BackpackItem && stack.hasNbt() && NbtReader.query(stack.nbt!!).let {
                it[BlockItem.BLOCK_ENTITY_TAG_KEY]["Backpack"]["UUID"].asUuid == state?.uuid
            }
        }
        return (hasNestingUpgrade || !isStorableItem()) && !isCurrentBackpack()
    }
}