package moe.fal.naive_backpacks.backpack

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.upgrade.ItemFilter
import moe.fal.naive_backpacks.upgrade.UpgradeInventory
import moe.fal.naive_backpacks.utils.nbt.NbtBuilder
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.item.ItemStack
import net.minecraft.nbt.NbtCompound
import net.minecraft.nbt.NbtElement
import net.minecraft.nbt.NbtList
import net.minecraft.server.MinecraftServer
import net.minecraft.util.Identifier
import net.minecraft.util.WorldSavePath
import net.minecraft.util.collection.DefaultedList
import net.minecraft.world.PersistentState
import net.minecraft.world.World
import java.util.*
import kotlin.io.path.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.deleteIfExists

class BackpackDataManager(server: MinecraftServer) {
    private val manager = server.getWorld(World.OVERWORLD)!!.persistentStateManager
    private val dataDir = server.getSavePath(WorldSavePath.ROOT)
        .parent
        .resolve(Path("data", MOD_ID))

    init {
        try {
            dataDir.createDirectory()
        } catch (_: Exception) {
        }
    }

    fun getOrCreate(backpack: AbstractBackpack): State {
        return manager.getOrCreate(
            State::fromNbt,
            { State.fromBackpack(backpack) },
            Path(MOD_ID, backpack.uuid.toString()).toString()
        )
    }

    fun getOrNull(uuid: UUID): State? {
        return manager.get(State::fromNbt, Path(MOD_ID, uuid.toString()).toString())
    }

    fun runAndRemove(uuid: UUID, f: State.() -> Unit): Boolean {
        val state = manager.get(
            State::fromNbt,
            Path(MOD_ID, uuid.toString()).toString()
        ) ?: return false
        state.f()
        state.markDirty()
        dataDir.resolve("$uuid.dat").deleteIfExists()
        return true
    }

    class State(
        val kind: BackpackKind,
        val uuid: UUID,
        contentList: DefaultedList<ItemStack>,
        upgradeList: DefaultedList<ItemStack>,
        val activeUpgrade: Int?,
        val filters: Map<Identifier, ItemFilter>,
    ) : PersistentState() {
        val content = BackpackInventory(contentList, this)
        val upgrades = UpgradeInventory(upgradeList, this)

        companion object {
            fun fromBackpack(backpack: AbstractBackpack): State {
                val uuid = backpack.uuid
                val kind = backpack.kind
                val content = DefaultedList.ofSize(kind.contentSize(), ItemStack.EMPTY)
                val upgrades = DefaultedList.ofSize(kind.maxUpgradeCount(), ItemStack.EMPTY)
                val activeUpgrade = null
                val filters = mapOf<Identifier, ItemFilter>(
                    Identifier(MOD_ID, "default") to ItemFilter()
                )

                return State(kind, uuid, content, upgrades, activeUpgrade, filters)
            }

            fun fromNbt(nbt: NbtCompound): State {
                val kindVal = NbtReader.newVarBy(BackpackKind::valueOf)
                val uuidVal = NbtReader.newVar<UUID>()
                val activeUpgradeVal = NbtReader.newVar<Int>()
                var filtersVal = NbtReader.newVarBy<NbtElement, _> { it as NbtList }

                var contentList: DefaultedList<ItemStack>? = null
                var upgradesList: DefaultedList<ItemStack>? = null


                NbtReader.scanCompound(nbt) {
                    "Kind" to kindVal

                    contentList = DefaultedList.ofSize(kindVal.unwrap().contentSize(), ItemStack.EMPTY)
                    upgradesList = DefaultedList.ofSize(kindVal.unwrap().maxUpgradeCount(), ItemStack.EMPTY)

                    "Content" to contentList!!
                    "UUID" to uuidVal
                    "Upgrade" to compoundOf {
                        "Upgrades" to upgradesList!!
                        "ActiveUpgrade" tryTo activeUpgradeVal
                        "Filters" to filtersVal
                    }
                }

                val filters = mutableMapOf<Identifier, ItemFilter>()
                NbtReader.forEach(filtersVal.unwrap()) { idAndFilter ->
                    val id = NbtReader.newVarBy<String, _> { Identifier(it) }
                    val filter = NbtReader.newVarBy<NbtElement, _> {
                        ItemFilter.fromNbt(it as NbtCompound)
                    }
                    NbtReader.scanCompound(idAndFilter as NbtCompound) {
                        "Id" to id
                        "Filter" to filter
                    }

                    filters[id.unwrap()] = filter.unwrap()
                }

                return State(
                    kindVal.unwrap(),
                    uuidVal.unwrap(),
                    contentList!!,
                    upgradesList!!,
                    activeUpgradeVal.tryUnwrap(),
                    filters
                )
            }
        }

        override fun writeNbt(nbt: NbtCompound): NbtCompound = nbt.also {
            NbtBuilder.merge(it) {
                "Kind" to kind.toString()
                "Content" to content.asList
                "UUID" to uuid
                "Upgrade" to compoundOf {
                    "Upgrades" to upgrades.asList
                    "ActiveUpgrade" tryTo activeUpgrade

                    "Filters" to listOf {
                        for ((id, filter) in filters) {
                            +compoundOf {
                                "Id" to id.toString()
                                "Filter" to filter.toNbt()
                            }
                        }
                    }
                }
            }
        }
    }
}