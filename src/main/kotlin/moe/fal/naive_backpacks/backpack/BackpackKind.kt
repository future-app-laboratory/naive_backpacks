package moe.fal.naive_backpacks.backpack

enum class BackpackKind {
    SMALL,
    MEDIUM,
    LARGE,
    EXTRA_LARGE;

    fun contentSize(): Int {
        return when (this) {
            SMALL -> 18
            MEDIUM -> 36
            LARGE -> 54
            EXTRA_LARGE -> 72
        }
    }

    fun rows(): Int {
        return when (this) {
            SMALL -> 2
            MEDIUM -> 4
            LARGE -> 6
            EXTRA_LARGE -> 8
        }
    }

    fun maxUpgradeCount(): Int {
        return when (this) {
            SMALL -> 2
            MEDIUM -> 4
            LARGE -> 6
            EXTRA_LARGE -> 8
        }
    }
}