package moe.fal.naive_backpacks.backpack

import moe.fal.naive_backpacks.item.BackpackItem
import moe.fal.naive_backpacks.utils.nbt.NbtReader
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import java.util.*


class ItemStackBackpack(private val stack: ItemStack) : AbstractBackpack {
    override val kind = (stack.item as BackpackItem).block.kind
    override val uuid: UUID

    override fun getName(): Text = stack.name
    override fun hasCustomName() = stack.hasCustomName()
    override fun getCustomName() = if (hasCustomName()) stack.name else null

    init {
        if (!stack.hasNbt()) {
            stack.nbt = stack.item.defaultStack.nbt
        }
        uuid = NbtReader.query(stack.nbt!!).let {
            it[BlockItem.BLOCK_ENTITY_TAG_KEY]["Backpack"]["UUID"].asUuid
        }
    }

    companion object {
        @JvmStatic
        fun findIn(playerInventory: PlayerInventory): ItemStackBackpack? {
            for (itemStack in listOf(playerInventory.armor, playerInventory.main).flatten()) {
                if (itemStack.item is BackpackItem) {
                    return ItemStackBackpack(itemStack)
                }
            }
            return null
        }
    }
}