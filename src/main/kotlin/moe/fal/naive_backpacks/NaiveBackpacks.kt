package moe.fal.naive_backpacks

import moe.fal.naive_backpacks.block.ModBlocks
import moe.fal.naive_backpacks.block.entity.ModBlockEntities
import moe.fal.naive_backpacks.backpack.ItemStackBackpack
import moe.fal.naive_backpacks.item.ModItems
import moe.fal.naive_backpacks.screen.BackpackScreenHandlerFactory
import moe.fal.naive_backpacks.screen.ModScreenHandlers
import net.fabricmc.api.ModInitializer
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking
import net.minecraft.util.Identifier
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object NaiveBackpacks : ModInitializer {
    const val MOD_ID = "naive_backpacks"
    val LOGGER: Logger = LoggerFactory.getLogger(MOD_ID)

    override fun onInitialize() {
        LOGGER.info("Initializing...")

        ModSounds.register()
        ModBlocks.register()
        ModItems.register()
        ModBlockEntities.register()
        ModScreenHandlers.register()

        ServerPlayNetworking.registerGlobalReceiver(Identifier(MOD_ID, "open")) { _, player, _, _, _ ->
            val backpack = ItemStackBackpack.findIn(player.inventory)
            if (backpack != null) {
                player.openHandledScreen(BackpackScreenHandlerFactory(backpack))
            }
        }
        LOGGER.info("Initialized.")
    }
}