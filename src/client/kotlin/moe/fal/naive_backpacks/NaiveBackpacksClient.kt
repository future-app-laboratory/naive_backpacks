package moe.fal.naive_backpacks

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.screen.BackpackScreen
import moe.fal.naive_backpacks.screen.ModScreenHandlers
import net.fabricmc.api.ClientModInitializer
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs
import net.minecraft.client.gui.screen.ingame.HandledScreens
import net.minecraft.client.option.KeyBinding
import net.minecraft.client.util.InputUtil
import net.minecraft.util.Identifier
import org.lwjgl.glfw.GLFW

object NaiveBackpacksClient : ClientModInitializer {
    private val keyBinding = KeyBindingHelper.registerKeyBinding(
        KeyBinding(
            "key.$MOD_ID.open",
            InputUtil.Type.KEYSYM,
            GLFW.GLFW_KEY_LEFT_ALT,
            "category.$MOD_ID.key"
        )
    )

    override fun onInitializeClient() {
        HandledScreens.register(ModScreenHandlers.BACKPACK_SCREEN_HANDLER_TYPE, ::BackpackScreen)

        ClientTickEvents.END_CLIENT_TICK.register {
            while (keyBinding.wasPressed()) {
                val buf = PacketByteBufs.empty()
                ClientPlayNetworking.send(Identifier(MOD_ID, "open"), buf)
            }
        }

//        BlockEntityRendererRegistry.register(ModBlockEntities.BACKPACK, ::BackpackRenderer);
    }
}