package moe.fal.naive_backpacks.screen

import moe.fal.naive_backpacks.NaiveBackpacks.MOD_ID
import moe.fal.naive_backpacks.backpack.BackpackKind
import net.fabricmc.api.EnvType
import net.fabricmc.api.Environment
import net.minecraft.client.gui.DrawContext
import net.minecraft.client.gui.screen.ingame.HandledScreen
import net.minecraft.entity.player.PlayerInventory
import net.minecraft.item.ItemStack
import net.minecraft.text.Text
import net.minecraft.util.Identifier
import kotlin.math.min


@Environment(value = EnvType.CLIENT)
class BackpackScreen(handler: BackpackScreenHandler, inventory: PlayerInventory, title: Text) :
    HandledScreen<BackpackScreenHandler>(handler, inventory, title) {
    companion object {
        val SMALL_TEXTURE = Identifier(MOD_ID, "textures/gui/small.png")
        val MEDIUM_TEXTURE = Identifier(MOD_ID, "textures/gui/medium.png")
        val LARGE_TEXTURE = Identifier(MOD_ID, "textures/gui/large.png")
        val EXTRA_LARGE_TEXTURE = Identifier(MOD_ID, "textures/gui/extra_large.png")
    }

    private val kind: BackpackKind

    init {
        kind = handler.backpack.kind

        titleX = 28
        titleY = 6
        playerInventoryTitleX = 28
        backgroundHeight = min(256, (150 - 18) + kind.rows() * 18)
        backgroundWidth = 216
        playerInventoryTitleY = backgroundHeight - 112
        if (kind == BackpackKind.EXTRA_LARGE) {
            playerInventoryTitleY += 19
        }
    }

    override fun render(context: DrawContext?, mouseX: Int, mouseY: Int, delta: Float) {
        renderBackground(context)
        super.render(context, mouseX, mouseY, delta)
        drawMouseoverTooltip(context, mouseX, mouseY)
    }

    override fun drawBackground(context: DrawContext?, delta: Float, mouseX: Int, mouseY: Int) {
        val texture = when (kind) {
            BackpackKind.SMALL -> SMALL_TEXTURE
            BackpackKind.MEDIUM -> MEDIUM_TEXTURE
            BackpackKind.LARGE -> LARGE_TEXTURE
            BackpackKind.EXTRA_LARGE -> EXTRA_LARGE_TEXTURE
        }
        context?.drawTexture(texture, x, y, 0, 0, backgroundWidth, backgroundHeight)
    }

    override fun getTooltipFromItem(stack: ItemStack?): MutableList<Text> {
        return super.getTooltipFromItem(stack)
    }
}